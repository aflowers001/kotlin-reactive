package com.flatcat.reactive.persistence

data class Mapping(var name: String? = null, var mapping: String? = null)
